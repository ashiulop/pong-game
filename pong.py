import pygame
import random

pygame.init()

# create the screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 500
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

# colors
WHITE = (255, 255, 255)
GRAY = (30, 30, 30)

# Game title and icon
pygame.display.set_caption("Pong")
icon = pygame.image.load('joystick.png')
pygame.display.set_icon(icon)

# player dimensions
player_width = 10
player_height = 80
player_speed = 7

# player1 coordinates
player1_x = 25
player1_y = (SCREEN_HEIGHT / 2) - player_height / 2

# player2 coordinates
player2_x = SCREEN_WIDTH - player_width - 25
player2_y = (SCREEN_HEIGHT / 2) - player_height / 2

# ball coordinates
ball_x = SCREEN_WIDTH // 2
ball_y = SCREEN_HEIGHT // 2
speeds = [-4, -3, -2, 2, 3, 4]
ball_speed_x = random.choice(speeds)
ball_speed_y = random.choice(speeds)
ball_radius = 6

# font for when the game is not started or paused
over_font = pygame.font.Font('freesansbold.ttf', 40)

# font for the score
font = pygame.font.Font('freesansbold.ttf', 20)

player1_score = 0
player2_score = 0


# function to only start the game when enter is pressed
def start_game():
    not_started = True
    while not_started:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                not_started = False
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    not_started = False

        screen.fill(GRAY)
        over_text = over_font.render("PRESS ENTER TO START", True, (255, 255, 255))
        screen.blit(over_text, (150, 210))

        pygame.display.update()
        clock.tick(10)


# function to pause the game when space is pressed
def pause():
    paused = True

    while paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                paused = False
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    paused = False

        screen.fill(GRAY)
        over_text = over_font.render("PRESS SPACE TO CONTINUE", True, (255, 255, 255))
        screen.blit(over_text, (100, 210))

        pygame.display.update()
        clock.tick(10)


# function for game over
def game_over():
    is_over = True
    while is_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_over = False
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    is_over = False

        screen.fill(GRAY)
        over_text1 = over_font.render("GAME OVER", True, (255, 255, 255))
        screen.blit(over_text1, (280, 160))

        over_text2 = over_font.render("Press ENTER to start again", True, (255, 255, 255))
        screen.blit(over_text2, (130, 250))

        pygame.display.update()
        clock.tick(10)


# defining the player class
class Player(object):
    def __init__(self, x, y):
        self.player = pygame.rect.Rect(x, y, player_width, player_height)

    def draw(self, surface):
        pygame.draw.rect(surface, WHITE, self.player)


# defining the ball class
class Ball(object):
    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius

    def display(self, surface):
        pygame.draw.circle(surface, WHITE, (self.x, self.y), self.radius)


clock = pygame.time.Clock()

start_game()
# Game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                pause()

    # moving the players
    key = pygame.key.get_pressed()
    if key[pygame.K_w]:
        player1_y += -player_speed
    elif key[pygame.K_s]:
        player1_y += player_speed

    if key[pygame.K_UP]:
        player2_y += -player_speed
    elif key[pygame.K_DOWN]:
        player2_y += player_speed

    screen.fill(GRAY)

    player1 = Player(player1_x, player1_y)
    player1.draw(screen)

    player2 = Player(player2_x, player2_y)
    player2.draw(screen)

    # constraining the players inside the screen
    if player1_y <= 0:
        player1_y = 0
    elif player1_y >= SCREEN_HEIGHT - player_height:
        player1_y = SCREEN_HEIGHT - player_height

    if player2_y <= 0:
        player2_y = 0
    elif player2_y >= SCREEN_HEIGHT - player_height:
        player2_y = SCREEN_HEIGHT - player_height

    ball = Ball(ball_x, ball_y, ball_radius)
    ball.display(screen)

    ball_x += ball_speed_x
    ball_y += ball_speed_y

    # contain the ball within the top and bottom of the screen
    if ball_y - ball_radius <= 0 or ball_y + ball_radius >= SCREEN_HEIGHT:
        ball_speed_y = -ball_speed_y

    # reset the ball when it hits the left or right wall and increment the score
    if ball_x - ball_radius <= 0:
        ball_x = SCREEN_WIDTH // 2
        ball_y = SCREEN_HEIGHT // 2
        ball_speed_x = random.choice(speeds)
        ball_speed_y = random.choice(speeds)
        player2_score += 1
    elif ball_x + ball_radius >= SCREEN_WIDTH:
        ball_x = SCREEN_WIDTH // 2
        ball_y = SCREEN_HEIGHT // 2
        ball_speed_x = random.choice(speeds)
        ball_speed_y = random.choice(speeds)
        player1_score += 1

    # checks if the ball hits one of the padels
    if (ball_x - ball_radius) <= (player1_x + player_width):
        if (ball_y - ball_radius) >= player1_y and (ball_y + ball_radius) <= (player1_y + player_height):
            ball_speed_x = -ball_speed_x
            if ball_speed_x > 0:
                ball_speed_x += 1
            else:
                ball_speed_x -= 1

    if (ball_x + ball_radius) >= player2_x:
        if (ball_y - ball_radius) >= player2_y and (ball_y + ball_radius) <= (player2_y + player_height):
            ball_speed_x = -ball_speed_x
            if ball_speed_x > 0:
                ball_speed_x += 1
            else:
                ball_speed_x -= 1

    # showing the score on the screen
    p1_score = font.render(player1_score.__str__(), True, (255, 255, 255))
    screen.blit(p1_score, (5, 5))

    p2_score = font.render(player2_score.__str__(), True, (255, 255, 255))
    screen.blit(p2_score, (SCREEN_WIDTH - 18, 5))

    # show the game over screen if one of the scores reaches 10
    if player1_score == 10 or player2_score == 10:
        game_over()
        player1_score = 0
        player2_score = 0

    pygame.display.update()

    clock.tick(60)